# Exploration de la PokéAPI avec Node.js

## Get Started

1. Lancer `npm install`
2. `npm start` pour voir les résultats

## Objectif

Réaliser un script en Node.js qui permette de récupérer les 50 premiers pokémons de la _[PokéAPI](https://pokeapi.co/)_ et qui les groupe par type.

## Contraintes

Votre script ne doit utiliser que les 51 URLs suivantes pour récupérer les données sur les pokémons :

- https://pokeapi.co/api/v2/pokemon?limit=50
- https://pokeapi.co/api/v2/pokemon/1/
- https://pokeapi.co/api/v2/pokemon/2/
- https://pokeapi.co/api/v2/pokemon/3/
- ...
- https://pokeapi.co/api/v2/pokemon/50/

## Exemple de résultat

![image](./exemple-de-resultat.png)

## Aide

Vous pouvez utiliser les bibliothèques [`got`](https://www.npmjs.com/package/got) ou [`ky`](https://www.npmjs.com/package/ky) pour requêter les URLs.

Vous pouvez utiliser un objet `Map` ([documentation](https://developer.mozilla.org/fr/docs/Web/JavaScript/Reference/Global_Objects/Map)) pour stocker les résultats (mais ce n'est pas obligatoire).

Pour contrôler vos résultats, vous pouvez consulter :

- https://pokeapi.co/api/v2/type/grass pour les pokémons de type _grass_
- https://pokeapi.co/api/v2/type/poison pour les pokémons de type _poison_
- ...

Bonne chance :wink:
