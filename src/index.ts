import got from 'got'

type Data = {
    name: string
    url: string
};

type Type = {
    slot: number
    type: Data
}

type Pokemon = {
    name: string
    type: Type
}

async function getPokemon(): Promise<Data[]> {
    const data = await got.get('https://pokeapi.co/api/v2/pokemon?limit=50')
    const {body} = data
    const {results} = JSON.parse(body)
    return results
}

async function getPokemonData(): Promise<Pokemon[]> {
    const res: Data[] = await getPokemon()
    const info = res.map(async (e: Data, i: number): Promise<Pokemon> => {
        const data = await got.get(e.url)
        const {body} = data
        const obj: Pokemon = {
            name: JSON.parse(body).name,
            type: JSON.parse(body).types
        }
        return obj
    })
    const data: Pokemon[] = await Promise.all(info)
    return data
}


const uniqueCategory = async (): Promise<string[]> => {
    const data: Pokemon[] = await getPokemonData()
    const category: string[] = []
    const allCategory = data.map((e: Pokemon): Type => {
        return e.type
    })
    allCategory.map((e: Type): void => {
        // @ts-ignore
        e.map((el: Type): void => {
            category.push(el.type.name)
        });
    })
    const unique = new Set(category);
    const newUnique: string[] = [...unique]
    return newUnique
}

const dictionaryPokemon = async (): Promise<any> => {
    const data: string[] = await uniqueCategory()
    const pokemon: Pokemon[] = await getPokemonData()
    const myMap = new Map([])
    data.forEach((e: string): void => {
        myMap.set(e, [])
    })
    for (let key of myMap) {
        const array = key[1]
        pokemon.map((e:Pokemon): void => {
            // @ts-ignore
            e.type.map((el: Type):void => el.type.name === key[0] ? array.push(e.name) : array.push())
        })
    }
    return myMap
}

dictionaryPokemon().then(r => console.log(r))